#
# Declare the repositories we know about:
#

create repository desktop-base.git
end repository

# Ignore delete / recreation of /packages/desktop-base
match /packages/
  min revision 18
  max revision 19
  action ignore
end match

# Import structure /package/desktop-base as master before it was moved to a
# more standard trunk/branches/tags
match /packages/desktop-base/
  max revision 35
  repository desktop-base.git
  branch master
end match

# Ignore failed try to move /packages/desktop-base into
# /packages/unstable/desktop-base
match /packages/
  min revision 36
  max revision 38
  action ignore
end match

# Continue import of /package/unstable/desktop-base as master before it was
# moved to a more standard trunk/branches/tags
match /packages/unstable/desktop-base/
  repository desktop-base.git
  branch master
end match

# Ignore single commit branches desktop-base-image-replacement-infrastructure
# and  desktop-base-moreblue-splashpack
match /packages/unstable/(desktop-base-image-replacement-infrastructure|desktop-base-moreblue-splashpack)/
  action ignore
end match
match /packages/trunk/(desktop-base-image-replacement-infrastructure|desktop-base-moreblue-splashpack)/
  action ignore
end match
match /packages/branches/stretch/(desktop-base-image-replacement-infrastructure|desktop-base-moreblue-splashpack)/
  action ignore
end match


# Import experimental branch
match /packages/experimental/desktop-base/
  repository desktop-base.git
  branch experimental
end match

# Import more standard trunk as master, once trunk/branches/tags structure is
# used
match /packages/trunk/desktop-base/
  repository desktop-base.git
  branch master
end match

# The stretch branch is later renamed stretch-abandoned and recreated from
# trunk. Import these revisions as stretch-abandoned directly to avoid
# confusing the import tool.
match /packages/branches/([^/]+)/desktop-base/
  min revision 447
  max revision 462
  repository desktop-base.git
  branch stretch-abandoned
end match

# Ignore stretch -> stretch-abandoned renaming, handled above
match /packages/branches/([^/]+)/desktop-base/
  min revision 463
  max revision 463
  action ignore
end match

# Import more standard branches, once trunk/branches/tags structure is used
match /packages/branches/([^/]+)/desktop-base/
  repository desktop-base.git
  branch \1
end match

# Ignore incorrect first 5.0.6 tag creation and deletion
match /packages/tags/
  min revision 219
  max revision 220
  action ignore
end match

# Ignore incorrect first 9.0.1 tag creation and deletion
match /packages/tags/
  min revision 434
  max revision 436
  action ignore
end match

# Ignore deletion of tag 9.0.2 that should have been a copy to stretch branch
match /packages/tags/desktop-base/([^/~]+)/
  min revision 465
  max revision 465
  action ignore
end match
# Import tags that are not real copies and have multiple parents as
# prepare-${branch} branches.
# These will need post-processing to create the git tags.
match /packages/tags/desktop-base/(4\.0\.6|4\.0\.7|5\.0\.0|5\.0\.1|6\.0\.5squeeze1|6\.0\.6|6\.0\.7|8\.0\.0|8\.0\.2)/
  repository desktop-base.git
  branch prepare-\1
end match
# Import clean tags as ref/tags
match /packages/tags/desktop-base/([^/~]+)/
  repository desktop-base.git
  branch refs/tags/\1
end match
# s/~/_/ in tag names for experimental versions.
# These also have multiple parents and need to be converted as
# prepare-${branch} branches, then post-processed to create the tags.
match /packages/tags/desktop-base/([^/~]+)~([^/~]+)/
  repository desktop-base.git
  branch prepare-\1_\2
end match
