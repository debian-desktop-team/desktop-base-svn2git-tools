#!/bin/bash
set -e

SVN_REPO=${1:-desktop-base}
SVN_REPO_DIR=/var/svn/${SVN_REPO}
echo "Repository name is '${SVN_REPO}'"
echo "  local dir: ${SVN_REPO_DIR}"

if [ ! -e ${SVN_REPO_DIR} ] ; then
  echo "Error: repository '${SVN_REPO_DIR}' not found"
  exit 1
fi

# Pre-import cleanup
rm -fr desktop-base.git/ gitlog-desktop-base.git log-desktop-base.git svn-all-fast-export.log

# Import from svn
svn-all-fast-export --identity-map=authors.txt --rules=desktop-base.rules --stats --debug-rules ${SVN_REPO_DIR} 2>&1 | tee svn-all-fast-export.log | grep WARN

# Post-process tags where necessary
for branch in $(git --git-dir=desktop-base.git branch -a | grep --color=none prepare) ; do
  tag_name=${branch/prepare-/}
  tag_command="git --git-dir=desktop-base.git tag $tag_name $branch"
  echo "Running: ${tag_command}"
  $(${tag_command})
done
